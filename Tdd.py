import unittest
import sys
import nltk



class TestStringMethods(unittest.TestCase):

	def test(self):
		nltk.download('stopwords')
		stopwords = nltk.corpus.stopwords.words('portuguese')
		arquivo = open("sem_stop.txt","r")
		texto = arquivo.read()
		texto = texto.split(" ")
		arquivo.close()
		flag = True
		for palavra in texto:
			if palavra in stopwords :
				flag = False
		self.assertEqual(flag , True)

def runTests():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestStringMethods)
    unittest.TextTestRunner(verbosity=2, failfast=True).run(suite)


if __name__ == '__main__':
	unittest.main()


